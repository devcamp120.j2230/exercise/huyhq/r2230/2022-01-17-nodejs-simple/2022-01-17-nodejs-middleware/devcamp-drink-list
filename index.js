//khai báo thư viện express
const express = require("express");

const {drinkRoute} = require("./routes/drinkRoute");
//khởi tạo app NodeJs
const app = express();

//khai báo cổng chạy ứng dụng
const port = 8000;

app.use("/",drinkRoute);

//chạy ứng dụng trên cổng
app.listen(port, () => {
    console.log("App running on port: " + port);
})