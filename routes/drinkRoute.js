const express = require("express");
const { Drink } = require("../Drink");
const {
    getAllDrink,
    getADrink,
    postADrink,
    putADrink,
    deleteADrink
} = require("../middlewares/drinkMiddleware");

const drinkRoute = express.Router();

var drink1 = new Drink("TRATAC", "Trà tắc", 10000, "14/5/2021");
var drink2 = new Drink("COCA", "Cocacola", 15000, "14/5/2021");
var drink3 = new Drink("PEPSI", "Pepsi", 10000, "14/5/2021");
var drink = {
    drink1,
    drink2,
    drink3,
};

drinkRoute.get("/",getAllDrink,(req,res)=>{
    res.status(200).json({
        message: "Get All Drinks!",
        drink
    })
})

drinkRoute.get("/:id",getADrink,(req,res)=>{
    var id = req.params.id;
    res.status(200).json({
        message: "Get Drink id = "+id,
    })
})

drinkRoute.post("/:id",postADrink,(req,res)=>{
    var id = req.params.id;
    res.status(200).json({
        message: "Create Drink id = "+id,
    })
})

drinkRoute.put("/:id",putADrink,(req,res)=>{
    var id = req.params.id;
    res.status(200).json({
        message: "Update Drink id = "+id,
    })
})

drinkRoute.delete("/:id",deleteADrink,(req,res)=>{
    var id = req.params.id;
    res.status(200).json({
        message: "Delete Drink id = "+id,
    })
})

module.exports = {drinkRoute};